﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Default : System.Web.UI.Page
    {
        private string cadena;
        private SqlConnection con;
        private DataSet ds;
        private SqlDataAdapter adapter;
        private SqlCommand command;
        private SqlCommandBuilder commandbuilder;

        protected void Page_Load(object sender, EventArgs e)
        {
            inicializarElementos();
            cargarTabla();
            actualizarNeto();
        }

        private void inicializarElementos()
        {
            cadena = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Admin\source\repos\Web\WebApplication1\WebApplication1\App_Data\datos.mdf;Integrated Security=True";
            con = new SqlConnection(cadena);
            command = new SqlCommand("select * from libro_contable order by fecha_y_hora desc", con);
            adapter = new SqlDataAdapter(command);
            commandbuilder = new SqlCommandBuilder(adapter);
            adapter.UpdateCommand = commandbuilder.GetUpdateCommand();
            //adapter.DeleteCommand = commandbuilder.GetDeleteCommand();
            //adapter.InsertCommand = commandbuilder.GetInsertCommand();
        }

        private void cargarTabla()
        {
            ds = new DataSet();
            adapter.Fill(ds);
            var dt = crearDataTable();

            dt = llenarDataTable(dt);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        private DataTable crearDataTable()
        {
            DataTable dt = new DataTable();
            DataColumn column;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ID";
            dt.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Fecha y Hora";
            dt.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Ingreso";
            dt.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Gasto";
            dt.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Concepto";
            dt.Columns.Add(column);
            return dt;
        }

        private DataTable llenarDataTable(DataTable dt)
        {
            foreach (DataRow dataSetRow in ds.Tables[0].Rows)
            {
                int id = Int32.Parse(dataSetRow[0].ToString());
                string monto = dataSetRow[3].ToString();
                string concepto = dataSetRow[4].ToString();
                bool ingreso;
                string fechaYHora = dataSetRow[1].ToString();
                ingreso = Boolean.Parse(dataSetRow[2].ToString());

                DataRow dtr = dt.NewRow();
                dtr["ID"] = id;
                dtr["Fecha y Hora"] = fechaYHora;
                dtr["Concepto"] = concepto;
                dtr["Ingreso"] = "";
                dtr["Gasto"] = "";
                if (ingreso)
                    dtr["Ingreso"] = monto;
                else
                    dtr["Gasto"] = monto;
                dt.Rows.Add(dtr);
            }
            return dt;
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            bool ingreso = (Request.Form["exampleRadios"] == "option1");
            string concepto = Request.Form["seleccionarConcepto"];
            int monto = Int32.Parse(Request.Form["monto"]);
            var fila = ds.Tables[0].NewRow();
            fila[1] = DateTime.Now.ToString();
            fila[2] = ingreso;
            fila[3] = monto;
            fila[4] = concepto;
            ds.Tables[0].Rows.Add(fila);
            adapter.Update(ds.Tables[0]);
            cargarTabla();
            actualizarNeto();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (GridView1.SelectedRow == null)
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mensaje de error", "alert('Error! debe seleccionar una fila')", true);
                else
                {
                    var rowIndex = GridView1.SelectedRow.RowIndex;
                    ds.Tables[0].Rows[rowIndex].Delete();
                    adapter.Update(ds.Tables[0]);
                    cargarTabla();
                    GridView1.SelectedIndex = -1;
                    //int id = Int32.Parse(row.Cells[1].Text);
                    actualizarNeto();
                    limpiarCamposEliminacion();
                }
            }
            catch (Exception err)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mensaje de error", "alert('Ocurrió un error desconocido: ' + '" + err.Message + "')", true);
            }
        }

        private void limpiarCamposEliminacion()
        {
            txtConcepto.Text = "";
            txtFechayHora.Text = "";
            txtId.Text = "";
            txtIngresoGasto.Text = "";
            Label3.Text = "Ingreso/Gasto";
        }

        private void actualizarNeto()
        {
            int gastos = 0;
            int ingresos = 0;
            foreach(DataRow row in ds.Tables[0].Rows)
            {
                if(Boolean.Parse(row[2].ToString()))
                    ingresos+=Int32.Parse(row[3].ToString());
                else
                    gastos += Int32.Parse(row[3].ToString());
            }
            int total = ingresos - gastos;
            Label1.Text = "$" + total;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = GridView1.SelectedIndex;
                txtId.Text = GridView1.SelectedRow.Cells[1].Text;
                txtFechayHora.Text = GridView1.SelectedRow.Cells[2].Text;
                if (GridView1.SelectedRow.Cells[3].Text == "&nbsp;")
                {
                    Label3.Text = "Gasto";
                    txtIngresoGasto.Text = GridView1.SelectedRow.Cells[4].Text;
                }
                else
                {
                    Label3.Text = "Ingreso";
                    txtIngresoGasto.Text = GridView1.SelectedRow.Cells[3].Text;
                }
                txtConcepto.Text = GridView1.SelectedRow.Cells[5].Text;

            }
            catch (Exception err)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Mensaje de error", "alert('Ocurrió un error desconocido: ' + '" + err.Message + "')", true);
            }
        }
    }
}