﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="./Estilos/Estilos.css" />
</head>
<body class="m-0">
    <h1 class="display-2 text-center titulo-principal bg-primary text-white p-4">Anotador Libro Contable Taxi</h1>
    <form id="form1" class="container" runat="server">
        <div class="row mx-0">
            <div class="col border rounded m-1">
                <h3 class="display-4 text-center">Agregar Nueva Fila</h3>
                <div class="form-group">
                    <label for="exampleInputEmail1">Monto</label>
                    <input type="number" name="monto" class="form-control form-control-lg" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingresar Monto"/>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input onclick="switchearSelect(this)" class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked="checked"/>
                        <label class="form-check-label" for="exampleRadios1">
                            Ingreso
                        </label>
                    </div>
                    <div class="form-check">
                        <input onclick="switchearSelect(this)" class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2"/>
                        <label class="form-check-label" for="exampleRadios2">
                            Gasto
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="seleccionarConcepto">Concepto</label>
                    <select name="seleccionarConcepto" id="seleccionarConcepto" class="form-control form-control-lg">
                        <option id='Viaje'>Viaje</option>
                        <option id='Otro'>Otro</option>
                    </select>
                </div>
        
                <asp:Button class="btn btn-primary mb-3" ID="btnAgregar" runat="server" OnClick="btnAgregar_Click" Text="Agregar" />
            </div>
            <div class="col m-1 d-flex align-items-center justify-content-center flex-wrap">
                <h3 class="display-4 text-white rounded-lg text-center bg-success p-2">Neto Actual:
                    <asp:Label class="" ID="Label1" runat="server" Text="Label"></asp:Label>
                </h3>
                
            </div>
        </div>
        <div class="border border-danger rounded p-3 mt-3">
            <h1 class="display-4 mb-0">Eliminar una fila:</h1> <span class="badge badge-danger">(debe seleccionar una fila del registro)</span> <div class="mb-3"></div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <asp:Label ID="Label2" runat="server" Text="ID"></asp:Label>
                    <asp:TextBox CssClass="form-control" ReadOnly="true" ID="txtId" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-md-3">
                    <asp:Label ID="Label5" runat="server" Text="Fecha y Hora"></asp:Label>
                    <asp:TextBox CssClass="form-control" ReadOnly="true" ID="txtFechayHora" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-md-3">
                    <asp:Label ID="Label3" runat="server" Text="Ingreso/Gasto"></asp:Label>
                    <asp:TextBox CssClass="form-control" ReadOnly="true" ID="txtIngresoGasto" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-md-4">
                    <asp:Label ID="Label4" runat="server" Text="Concepto"></asp:Label>
                    <asp:TextBox CssClass="form-control" ReadOnly="true" ID="txtConcepto" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="flex-container">
                <asp:Button class="btn btn-danger m-3" ID="btnEliminar" runat="server" Text="Eliminar" OnClick="btnEliminar_Click" OnClientClick="return confirm('está seguro que desea eliminar la fila seleccionada?')" />
            </div>  
        </div>
        <h3 class="display-4 text-center">Registro</h3>
        <asp:GridView ID="GridView1" CssClass="mb-5" runat="server" CellPadding="4" ForeColor="#333333" GridLines="Vertical" AutoGenerateSelectButton="True" Width="100%" Font-Size="Large" HorizontalAlign="Center" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" BorderStyle="Solid">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" HorizontalAlign="Center"/>
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Center"/>
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </form>
    
    <script src="./Scripts/Scripts.js">
    </script>
</body>
</html>
